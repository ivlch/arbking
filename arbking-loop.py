#!/usr/bin/env python

import urllib2
import json
from database import exchanges, cryptos, exchangesKrakenGdax
import time
from time import strftime, gmtime
import gmailer
import mailcreds


investment = 1000
asset = "ETH"
vehicle = "EUR"

#timing
runForInHours = 24*7
secondsBetweenData = 30

def launch():
	arb(runForInHours, secondsBetweenData)

def makePriceList(exchangesAnalysed):
	exchangesList = exchangesAnalysed.keys()
	numberOfExchanges = range(len(exchangesList))
	priceList = []

	for i in numberOfExchanges:
		price = getPrice(vehicle, asset, exchangesList[i])
	        
		if price != False:
			if not price == None:
				priceList.insert(i, price)
			else: 
				priceList.insert(i, None)

	for i in range(len(priceList)):
		if priceList[i] == False:
			print("Not connected to Internet.")
			break;
		else:
			return exchangesList, exchangesAnalysed, priceList

def arb(hours, delay):
	roiList = [0]
	print("-----------------------------")
	print "----- ARBKING",asset,"/", vehicle, "-----"
	print("-----------------------------")
	print("-----------------------------")
	print "INVESTMENT: ", investment, "\n"
	print "Analyzed for:"

	#for i in numberOfExchanges:
	#	print listedExchanges[i]
	#print "\n"

	period = (3600*hours)/delay

	print "RESULTS:"
	for i in range(period):

		makePriceListReturned = makePriceList(exchangesKrakenGdax)
		listedExchanges = makePriceListReturned[0]
		numberOfExchanges = makePriceListReturned[1]
		listedPrices = makePriceListReturned[2]

		for i in range(len(listedExchanges)):
			for j in range(len(listedExchanges)):
				if (i!=j):
					if listedPrices[i] < listedPrices[j] and listedPrices[i]!=None and listedPrices[j]!=None:
						print("********************************************************\n")
						print listedExchanges[i], ": ", "{:.5f}".format(listedPrices[i]), vehicle
						print listedExchanges[j], ": ", "{:.5f}".format(listedPrices[j]), vehicle
						print("--")

						profit_earned = profitFiat(asset, vehicle, listedExchanges[j], listedExchanges[i], listedPrices[j], listedPrices[i], investment)
						
						if (profit_earned["profit"]>0):
							if profit_earned["roi"]>roiList[0]:
								roiList[0] = listedExchanges[i], listedExchanges[j], "{:.5f}".format(profit_earned["roi"])

							with open("roi-ledger.txt", "a") as f:
								time_now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
								f.write(time_now + "," + asset + "," + vehicle + "," + listedExchanges[i] + "," + listedExchanges[j] + "," + "{:.6f}".format(profit_earned["number"]) + "," + "{:.5f}".format(profit_earned["spread"]) + "," + "{:.5f}".format(profit_earned["fee"]) + "," + "{:.5f}".format(profit_earned["profit"]) + "," + "{:.5f}".format(profit_earned["roi"]))
								f.write("\n")
								print("written to file: " + str(i))

							print "SPREAD: ", "{:.5f}".format(profit_earned["spread"]), vehicle
							print "No. of",asset, ":", "{:.6f}".format(profit_earned["number"])
							print "FEE: ", "{:.5f}".format(profit_earned["fee"]), vehicle
							print "PROFIT: ", "{:.5f}".format(profit_earned["profit"]), vehicle
							print "ROI: ", "{:.5f}".format(profit_earned["roi"]), "\n", "\n",

						#uncomment the lines below to also print unprofitable trades
						else:
							print "PROFIT IS NEGATIVE.", "\n", "\n"
						#print "FEE: ", "{:.4f}".format(profit_earned["fee"]), vehicle
						#print "PROFIT: ", "{:.4f}".format(profit_earned["profit"]), vehicle

					#if want to know about cases where price is higher between the exchange pair, uncomment the line below
					#else: print "***PRICE IS HIGHER.***", "\n", "\n"
		time.sleep(delay)

def profitFiat(cryptoAsset, cryptoVehicle, exchangeHigher, exchangeLower, priceHigher, priceLower, investment):
	n = investment/priceHigher
	fee = exchanges[exchangeHigher]["taker"]*investment + cryptos[cryptoAsset]["feeEur"] + n*priceHigher*exchanges[exchangeLower]["taker"] + exchanges[exchangeLower]["sepaEur"]
	spread = priceHigher-priceLower
	profit_earned = n * (priceHigher-priceLower) - fee
	roi = profit_earned/investment
	return {"profit":profit_earned, "fee":fee, "spread":spread, "roi":roi, "number":n, "spread":spread}

def profitCrypto(cryptoAsset, cryptoVehicle, exchangeHigher, exchangeLower, priceHigher, priceLower, investment):
	n = investment/priceHigher
	fee = exchanges[exchangeHigher]["taker"]*investment + cryptos[cryptoAsset]["feeCrypto"] + n*priceHigher*exchanges[exchangeLower]["taker"] + cryptos[cryptoVehicle]["feeCrypto"]
	spread = priceHigher-priceLower
	profit_earned = n * (priceHigher-priceLower) - fee
	roi = profit_earned/investment*100
	return {"profit":profit_earned, "fee":fee, "spread":spread, "roi":roi, "number":n, "spread":spread}


def getPrice(pricein, ticker, exchange):
	url = "https://min-api.cryptocompare.com/data/price?fsym=" + ticker + "&tsyms=" + pricein + "&e=" + exchange
	try:
		response = urllib2.urlopen(url)
		data = json.load(response)
		if not pricein in data:
			return None
		else: return data[pricein];
	except urllib2.URLError as err:
		return False

if __name__ == '__main__':
    launch()
    gmailer.timer()
