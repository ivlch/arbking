exchanges = {
	"GDAX": {
		"id": 0,
		"name": "GDAX",
		"maker": 0,
		"taker": 0.0030,
		"sepaEur": 0.15
	},
	"Kraken": {
		"id": 1,
		"name": "Kraken",
		"maker": 0.0016,
		"taker": 0.0026,
		"sepaEur": 0.15
	},
		"Bitstamp": {
		"id": 2,
		"name": "Bitstamp",
		"maker": 0.0025,
		"taker": 0.0025,
		"sepaEur": 0.90
	}
}

exchangesKrakenGdax = {
	"GDAX": {
		"id": 0,
		"name": "GDAX",
		"maker": 0,
		"taker": 0.0030,
		"sepaEur": 0.15
	},
	"Kraken": {
		"id": 1,
		"name": "Kraken",
		"maker": 0.0016,
		"taker": 0.0026,
		"sepaEur": 0.15
	}
}

cryptos = {
	"ETH": {
		"name": "Ethereum",
		"ticker": "ETH",
		"feeEur": 1,
		"feeCrypto": 0.001428
	},
	"BTC": {
		"name": "Bitcoin",
		"ticker": "BTC",
		"feeEur": 20,
		"feeCrypto": 0.001655
	},
	"LTC": {
		"name": "Litecoin",
		"ticker": "LTC",
		"feeEur": 1,
		"feeCrypto": 0.005
	}
}