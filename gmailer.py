# Import smtplib for the actual sending function
import smtplib
import mailcreds
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import time

def sendMail(fromaddr, password, toaddr, subject, body):
    fromaddr = mailcreds.email
    toaddr = toaddr

    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject
    
    msg.attach(MIMEText(body, 'plain'))
     
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()
