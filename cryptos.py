cryptos = {
	"ETH": {
		"name": "Ethereum",
		"ticker": "ETH",
		"feeEur": 1.5
	},
	"BTC": {
		"name": "Bitcoin",
		"ticker": "BTC",
		"feeEur": 15
	}
}